//
//  SetlistViewController.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 01/04/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import "SetlistViewController.h"
#import "SetlistGrabber.h"
#import "SpotifyPlayerViewController.h"
#import "PlaylistAddedConfirmationViewController.h"
#import <RestKit/Restkit.h>
#import "Venue.h"
#import <Spotify/Spotify.h>
#import "Config.h"

@interface SetlistViewController () <SPTAuthViewDelegate>

@property (atomic, readwrite) SPTAuthViewController *authViewController;
@property (atomic, readwrite) BOOL firstLoad;

@property (nonatomic) NSString *dayName;

@end

@implementation SetlistViewController

@synthesize theArtistLabel, theVenueLabel, theDateLabel, theMainSetField, dateOfShow, artist, venue;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUpCoreDataStack];
    
    [self.setlistSpinner setHidden:NO];
    [self.setlistSpinner startAnimating];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionUpdatedNotification:) name:@"sessionUpdated" object:nil];
    self.firstLoad = YES;
    
    [self roundSpotifyButtonCorners];
    
    NSInteger randomNumber = arc4random_uniform(4);
    NSString *imagename = [NSString stringWithFormat:@"%li", (long)randomNumber];
    self.backgroundImageView.image = [UIImage imageNamed:imagename];
    
    self.setlistImage.image = [UIImage imageNamed:@"setlist_image"];
    
    CGFloat imageHeight = self.setlistImage.bounds.size.height;
    self.topGapHeight.constant = imageHeight * 0.11;
    self.bottomGapHeight.constant = imageHeight * 0.07;
    CGFloat imageWidth = self.setlistImage.bounds.size.width;
    self.leftGapHeight.constant = imageWidth * 0.06;
    self.rightGapHeight.constant = imageWidth * 0.12;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(_setlist == nil){
        [self fetchSet];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.backgroundImageView.image = nil;
}

- (void)setSetlist
{
    if (![artist isEqual:@""] && [theMainSetField.text isEqual:@""]) {

        if(_setlist != nil)
        {
            [self.setlistSpinner stopAnimating];
            [self.setlistSpinner setHidden:YES];
            
            theArtistLabel.text = _setlist.artistName;
            theVenueLabel.text = _setlist.inVenue.name;
            theDateLabel.text = [_setlist dateDisplayString];
            
            theMainSetField.text = _setlist.setlistAsString;
        }
    }
    
    if(_setlist == nil || [theMainSetField.text isEqualToString:@""]){
        UIAlertView *noSetAlert = [[UIAlertView alloc] initWithTitle:@"No Results"
                                                             message:@"Sorry, we don't have the setlist for that show available."
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles: nil];
        [noSetAlert show];
    }
    
    [self.setlistSpinner stopAnimating];
    [self.setlistSpinner setHidden:YES];
}

- (void)fetchSet
{
    [self.setlistSpinner startAnimating]; // start the spinner
    
    SetlistGrabber *setGrabber = [[SetlistGrabber alloc]init];

    NSString *artistString = artist;
    artistString = [artistString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    [setGrabber fetchSetlistofArtist:artistString atVenue:venue onDate:dateOfShow inContext:self.managedObjectContext];
    
    NSFetchRequest *fetchSetRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Setlist"
                                              inManagedObjectContext:self.managedObjectContext];
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
    [fetchSetRequest setSortDescriptors:@[sortDesc]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"containsSets.@count > 0", self];
    [fetchSetRequest setEntity:entity];
    [fetchSetRequest setPredicate:predicate];
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchSetRequest error:nil]
    ;
    _setlist = fetchedObjects.firstObject;
    [self setSetlist];
    
    [self.setlistSpinner stopAnimating]; // stop the spinner
}

#pragma mark - spotify intergration

- (BOOL)prefersStatusBarHidden {
    return NO;
}

-(void)sessionUpdatedNotification:(NSNotification *)notification {
    if(self.navigationController.topViewController == self) {
        SPTAuth *auth = [SPTAuth defaultInstance];
        if (auth.session && [auth.session isValid]) {
            //[self performSegueWithIdentifier:@"ShowPlayer" sender:nil];
        }
    }
}

-(void)showPlayer {
//    self.firstLoad = NO;
//    
//    SpotifyPlayerViewController *viewController = (SpotifyPlayerViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SpotifyPlayer"];
//    viewController.artist = self.artist;
//    viewController.venue = self.venue;
//    viewController.dateOfShow = self.dayName;
//    viewController.setlist = [self checkForEncores:self.setlist];
//    
//    if (self.artist != nil && self.dayName)
//    {
//        [self.navigationController pushViewController:viewController animated:NO];
//        //[self.navigationController presentViewController:viewController animated:NO completion:nil];
//    }
//    
    self.firstLoad = NO;
    
    PlaylistAddedConfirmationViewController *viewController = (PlaylistAddedConfirmationViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PlaylistAddedConfirmation"];
    viewController.setlist = self.setlist;
    viewController.managedObjectContext = self.managedObjectContext;
    
    if (self.setlist != nil)
    {
        //[self.navigationController pushViewController:viewController animated:NO];
        //[self presentViewController:viewController animated:NO completion:nil];
        [viewController showInView:self.view animated:YES];
    }
}

- (NSArray*)checkForEncores:(NSArray *)set
{
    BOOL containsEncores = NO;
    
    for (id object in set) {
        if ([object isKindOfClass:[NSArray class]]) {
            containsEncores = YES;
            break;
        }
    }
    
    if (containsEncores)
    {
        NSMutableArray *totalSet = [[NSMutableArray alloc] init];
        
        for (id songs in set)
        {
            if ([songs isKindOfClass:[NSArray class]])
            {
                for (NSString *song in songs)
                {
                    [totalSet addObject:song];
                }
            }
            else if ([songs isKindOfClass:[NSString class]])
            {
                //check incase encore is only one song in which case it will be a lone string instead of an array
                [totalSet addObject:songs];
            }
            
        }
        
        return totalSet;
    }
    else
    {
        if ([set isKindOfClass:[NSString class]])
        {
            //check incase encore is only one song in which case it will be a lone string instead of an array
            return @[set];
        }
        else
        {
            return set;
        }
    }
}

- (void)authenticationViewController:(SPTAuthViewController *)viewcontroller didFailToLogin:(NSError *)error {
    NSLog(@"*** Failed to log in: %@", error);
}

- (void)authenticationViewController:(SPTAuthViewController *)viewcontroller didLoginWithSession:(SPTSession *)session {
    [self showPlayer];
}

- (void)authenticationViewControllerDidCancelLogin:(SPTAuthViewController *)authenticationViewController {
    //self.statusLabel.text = @"Login cancelled.";
}

- (void)openLoginPage {
    //self.statusLabel.text = @"Logging in...";
    
    self.authViewController = [SPTAuthViewController authenticationViewController];
    self.authViewController.delegate = self;
    self.authViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.authViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.definesPresentationContext = YES;
    
    [self presentViewController:self.authViewController animated:NO completion:nil];
}


- (void)renewTokenAndShowPlayer {
    //self.statusLabel.text = @"Refreshing token...";
    SPTAuth *auth = [SPTAuth defaultInstance];
    
    [auth renewSession:auth.session callback:^(NSError *error, SPTSession *session) {
        auth.session = session;
        
        if (error) {
            //self.statusLabel.text = @"Refreshing token failed.";
            NSLog(@"*** Error renewing session: %@", error);
            return;
        }
        
        [self showPlayer];
    }];
}

- (void)roundSpotifyButtonCorners
{
    //round corners of check box
    // Create the path
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.spotifyButton.bounds
                                                   byRoundingCorners:UIRectCornerAllCorners
                                                         cornerRadii:CGSizeMake(11.0, 11.0)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.spotifyButton.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    self.spotifyButton.layer.mask = maskLayer;
}

- (void)viewWillAppear:(BOOL)animated {
    SPTAuth *auth = [SPTAuth defaultInstance];
    
    // Check if we have a token at all
    if (auth.session == nil) {
        //self.statusLabel.text = @"";
        return;
    }
    
    // Check if it's still valid
    if ([auth.session isValid] && self.firstLoad) {
        // It's still valid, show the player.
        //[self showPlayer];
        return;
    }
    
    // Oh noes, the token has expired, if we have a token refresh service set up, we'll call tat one.
    //self.statusLabel.text = @"Token expired.";
    if (auth.hasTokenRefreshService) {
        [self renewTokenAndShowPlayer];
        return;
    }
    
    // Else, just show login dialog
}

- (IBAction)loginClicked:(id)sender {
    [self openLoginPage];
}

//- (IBAction)clearCookiesClicked:(id)sender {
//    self.authViewController = [SPTAuthViewController authenticationViewController];
//    [self.authViewController clearCookies:nil];
//    //self.statusLabel.text = @"Cookies cleared.";
//}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setUpCoreDataStack
{
    NSManagedObjectModel *model = [NSManagedObjectModel mergedModelFromBundles:[NSBundle allBundles]];
    
    NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    
    NSURL *url = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:@"Database.sqlite"];
    
    [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:nil error:nil];
    
    self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    
    self.managedObjectContext.persistentStoreCoordinator = psc;
}

- (IBAction)setlistSourceButtonPressed:(id)sender
{
    NSURL *url = [NSURL URLWithString:_setlist.setlistURL];
    [[UIApplication sharedApplication] openURL:url];
}

@end
