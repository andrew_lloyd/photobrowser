//
//  SetlistViewController.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 01/04/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Setlist.h"

@interface SetlistViewController : UIViewController<UIAlertViewDelegate>

@property(strong, nonatomic) NSString *dateOfShow;
@property(strong, nonatomic) Setlist *setlist;
@property(strong, nonatomic) NSString*artist;
@property(strong, nonatomic) NSString*venue;

@property (strong, nonatomic) IBOutlet UILabel *theArtistLabel;
@property (strong, nonatomic) IBOutlet UILabel *theVenueLabel;
@property (strong, nonatomic) IBOutlet UILabel *theDateLabel;
@property (strong, nonatomic) IBOutlet UITextView *theMainSetField;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *setlistSpinner;

@property (weak, nonatomic) IBOutlet UIButton *spotifyButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UITextView *setTextView;
@property (weak, nonatomic) IBOutlet UIImageView *setlistImage;
@property (weak, nonatomic) IBOutlet UIButton *setlistSourceButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topGapHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomGapHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftGapHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightGapHeight;

@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;

- (void)setSetlist:(NSArray*)setlistSongs;

@end
