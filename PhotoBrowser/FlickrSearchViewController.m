//
//  FlickrSearchViewController.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 17/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import "FlickrSearchViewController.h"

@interface FlickrSearchViewController ()


@end


@implementation FlickrSearchViewController

@synthesize searchField, searchVenueField, datePicker;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set nav bar properties
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:0 green:164.0/255.0 blue:180.0/255/.0 alpha:1.0]};
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0 green:184.0/255.0 blue:1 alpha:1.0];
    
    // Do any additional setup after loading the view.
    searchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    searchVenueField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    //sets maximum slectable date as current date, picking a future date wouldn't make sense
    [datePicker setMaximumDate:[NSDate date]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self prepareResultsViewController:segue.destinationViewController];
}


- (void)prepareResultsViewController:(FlickrResultsTableViewController *)tvc
{
    NSString *query = searchField.text;
    query = [query stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *venueQuery = searchVenueField.text;
    venueQuery = [venueQuery stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *finalQuery = [NSString stringWithFormat:@"%@+%@", query, venueQuery];
    tvc.query = finalQuery;
    tvc.dateTaken = datePicker.date;
    
    if ([query isEqualToString:@""]) {
        tvc.title = @"Search Results";
    }else{
        tvc.title = [NSString stringWithFormat:@"Results for %@", searchField.text];
    }
}

- (IBAction)searchBtn:(id)sender {
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [searchField resignFirstResponder];
    [searchVenueField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField) {
        [textField resignFirstResponder];
    }
    
    return NO;
}

@end
