//
//  ReviewViewController.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 05/06/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewViewController : UIViewController
@property (nonatomic, strong) NSURL *theURLquery; 
@property (nonatomic) IBOutlet UIWebView *theWebView;

@end
