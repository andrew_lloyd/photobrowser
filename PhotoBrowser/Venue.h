#import "_Venue.h"

@interface Venue : _Venue {}
// Custom logic goes here.

- (void)populateWithJSON:(NSDictionary*)json inContext:(NSManagedObjectContext*)context;

@end
