// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Set.m instead.

#import "_Set.h"

const struct SetAttributes SetAttributes = {
	.encore = @"encore",
};

const struct SetRelationships SetRelationships = {
	.containsSongs = @"containsSongs",
	.inSetlist = @"inSetlist",
};

@implementation SetID
@end

@implementation _Set

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Set" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Set";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Set" inManagedObjectContext:moc_];
}

- (SetID*)objectID {
	return (SetID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"encoreValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"encore"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic encore;

- (BOOL)encoreValue {
	NSNumber *result = [self encore];
	return [result boolValue];
}

- (void)setEncoreValue:(BOOL)value_ {
	[self setEncore:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveEncoreValue {
	NSNumber *result = [self primitiveEncore];
	return [result boolValue];
}

- (void)setPrimitiveEncoreValue:(BOOL)value_ {
	[self setPrimitiveEncore:[NSNumber numberWithBool:value_]];
}

@dynamic containsSongs;

- (NSMutableOrderedSet*)containsSongsSet {
	[self willAccessValueForKey:@"containsSongs"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"containsSongs"];

	[self didAccessValueForKey:@"containsSongs"];
	return result;
}

@dynamic inSetlist;

@end

@implementation _Set (ContainsSongsCoreDataGeneratedAccessors)
- (void)addContainsSongs:(NSOrderedSet*)value_ {
	[self.containsSongsSet unionOrderedSet:value_];
}
- (void)removeContainsSongs:(NSOrderedSet*)value_ {
	[self.containsSongsSet minusOrderedSet:value_];
}
- (void)addContainsSongsObject:(Song*)value_ {
	[self.containsSongsSet addObject:value_];
}
- (void)removeContainsSongsObject:(Song*)value_ {
	[self.containsSongsSet removeObject:value_];
}
- (void)insertObject:(Song*)value inContainsSongsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"containsSongs"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self containsSongs]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"containsSongs"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"containsSongs"];
}
- (void)removeObjectFromContainsSongsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"containsSongs"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self containsSongs]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"containsSongs"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"containsSongs"];
}
- (void)insertContainsSongs:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"containsSongs"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self containsSongs]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"containsSongs"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"containsSongs"];
}
- (void)removeContainsSongsAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"containsSongs"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self containsSongs]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"containsSongs"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"containsSongs"];
}
- (void)replaceObjectInContainsSongsAtIndex:(NSUInteger)idx withObject:(Song*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"containsSongs"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self containsSongs]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"containsSongs"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"containsSongs"];
}
- (void)replaceContainsSongsAtIndexes:(NSIndexSet *)indexes withContainsSongs:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"containsSongs"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self containsSongs]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"containsSongs"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"containsSongs"];
}
@end

