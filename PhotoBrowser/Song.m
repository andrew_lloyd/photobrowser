#import "Song.h"

@interface Song ()

// Private interface goes here.

@end

@implementation Song

// Custom logic goes here.
- (void)populateWithJSON:(NSDictionary*)json inContext:(NSManagedObjectContext*)context
{
    self.name = [json valueForKeyPath:@"name"];
    self.originalArtist = [json valueForKeyPath:@"cover.name"];
}

@end
