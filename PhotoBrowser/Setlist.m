#import "Setlist.h"
#import "Set.h"
#import "Song.h"
#import "Venue.h"

@interface Setlist ()

// Private interface goes here.

@end

@implementation Setlist

// Custom logic goes here.
- (void)populateWithJSON:(NSDictionary*)json inContext:(NSManagedObjectContext*)context
{
    self.artistName = [json valueForKeyPath:@"artist.name"];
    self.setlistURL = [json valueForKey:@"url"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+0:00"]];
    
    self.date = [dateFormatter dateFromString:[json valueForKeyPath:@"eventDate"]];
    
    Venue *venue = [NSEntityDescription
                insertNewObjectForEntityForName:@"Venue"
                inManagedObjectContext:context];
    
    
    NSDictionary *venueJSON = [json valueForKeyPath:@"venue"];
    [venue populateWithJSON:venueJSON inContext:context];
    self.inVenue = venue;
    
    id sets = [json valueForKeyPath:@"sets"];
    if ([sets isKindOfClass:[NSString class]])
    {
        if ([sets isEqualToString:@""])
        {
            return;
        }
    }
    else
    {
        NSDictionary *sets = [json valueForKeyPath:@"sets.set"];
        
        if (sets.count > 1)
        {
            for (NSDictionary *setJSON in sets)
            {
                //check set contains songs
                if ([setJSON isKindOfClass:[NSDictionary class]])
                {
                    Set *set = [NSEntityDescription
                                insertNewObjectForEntityForName:@"Set"
                                inManagedObjectContext:context];
                    
                    [set populateWithJSON:setJSON inContext:context];
                    [self.containsSetsSet addObject:set];
                }
            }
        }
        else
        {
            Set *set = [NSEntityDescription
                        insertNewObjectForEntityForName:@"Set"
                        inManagedObjectContext:context];
            
            [set populateWithJSON:sets inContext:context];
            [self.containsSetsSet addObject:set];
        }
    }
}

- (BOOL)containsSongs
{
    for (Set *set in self.containsSets)
    {
        if (set.containsSongs.count > 0)
        {
            return true;
        }
    }
    
    return false;
}


- (NSString*)setlistAsString
{
    NSString *result = @"";
    
    for (Set *set in self.containsSets)
    {
        result = [NSString stringWithFormat:@"%@\n", result];
        
        if (set.encoreValue)
        {
            result = [NSString stringWithFormat:@"%@\nEncore:", result];
        }
        
        for (Song *song in set.containsSongs)
        {
            result = [NSString stringWithFormat:@"%@\n%@", result, song.name];
        }
    }
    
    while ([result hasPrefix:@"\n"])
    {
        result = [result substringFromIndex:1];
    }
    
    return result;
}

- (NSString*)dateDisplayString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEEE, dd MMM yyyy"];
    
    return [dateFormatter stringFromDate:self.date];
}

- (NSArray*)arrayOfAllSongs
{
    NSMutableArray *result = [[NSMutableArray alloc] initWithArray:@[]];
    
    for (Set *set in self.containsSets)
    {
        for (Song *song in set.containsSongs)
        {
            [result addObject:song];
        }
    }
    
    return result;
}

- (BOOL)allSongsCheckedForSpotifyURI
{
    for (Set *set in self.containsSets)
    {
        for (Song *song in set.containsSongs)
        {
            if (song.spotifyTrackURI.length == 0)
            {
                return NO;
            }
        }
    }
    
    return YES;
}

@end
