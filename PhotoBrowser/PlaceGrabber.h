//
//  PlaceGrabber.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 20/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


#define GOOGLE_RESULTS_PLACES @"results.name" //JSON results

@interface PlaceGrabber : NSObject

@property (nonatomic, strong) NSArray *places;
@property (nonatomic, strong) NSString *eventName;
@property(nonatomic, strong)NSArray *eventArtists;


-(void)setPhotoLatitudeAndLongitude:(CLLocation *)location;
-(void)fetchPlace:(NSDate *)onDate;
-(NSArray *)getPlaces;
-(NSString *)getTheEvent;
-(NSArray *)getArtists;




@end

