//
//  PhotoBrowserViewController.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 06/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import <QuartzCore/QuartzCore.h>
#import <Social/Social.h>


@interface PhotoBrowserViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate, UIAlertViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic) NSMutableArray *selectedImages;
@property (nonatomic) UIImage *image;
@property (nonatomic, strong) CLLocation *storedPhotoLocation;
@property (nonatomic) NSDate *datePhotoWasTaken;
@property (nonatomic ,strong) NSArray *theArtists;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *downloadingSpinner;
@property (strong, nonatomic) IBOutlet UIButton *imageButton;
@property (strong, nonatomic) IBOutlet UIButton *facebookButton;
@property (strong, nonatomic) IBOutlet UIButton *tweetButton;
@property (strong, nonatomic) IBOutlet UIButton *browseOthersButton;
@property (strong, nonatomic) IBOutlet UIButton *reviewsButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *viewThisSetlist;
@property (strong, nonatomic) IBOutlet UILabel *artistLabel;
@property (strong, nonatomic) IBOutlet UILabel *venueLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (nonatomic) IBOutlet UIPickerView *bandPicker;

- (IBAction)takePhoto:(UIButton *)sender;
- (IBAction)selectPhoto:(UIButton *)sender;
- (IBAction)enlargeImage:(UIButton *)sender;

-(UIImage *)getImage;
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
-(NSArray* )getArtists;



@end
