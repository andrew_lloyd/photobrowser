//
//  HomeViewController.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 19/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import "HomeViewController.h"


@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSInteger randomNumber = arc4random_uniform(4);
    NSString *imagename = [NSString stringWithFormat:@"%li", (long)randomNumber];
    self.backgroundImageView.image = [UIImage imageNamed:imagename];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.backgroundImageView.image = nil;
}

@end
