#import "Venue.h"

@interface Venue ()

// Private interface goes here.

@end

@implementation Venue

// Custom logic goes here.
- (void)populateWithJSON:(NSDictionary*)json inContext:(NSManagedObjectContext*)context
{
    self.name = [json valueForKeyPath:@"name"];
    self.country = [json valueForKeyPath:@"city.country.name"];
    self.latitudeValue = [[json valueForKeyPath:@"city.coords.lat"] floatValue];
    self.longitudeValue = [[json valueForKeyPath:@"city.coords.long"] floatValue];
    
}

@end
