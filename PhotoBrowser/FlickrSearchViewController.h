//
//  FlickrSearchViewController.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 17/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickrResultsTableViewController.h"

@interface FlickrSearchViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *searchField;
@property (strong, nonatomic) IBOutlet UITextField *searchVenueField;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

- (IBAction)searchBtn:(id)sender;

- (void)prepareResultsViewController:(FlickrResultsTableViewController *)tvc;

@end
