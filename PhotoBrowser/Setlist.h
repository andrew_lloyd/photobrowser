#import "_Setlist.h"

@interface Setlist : _Setlist {}

- (void)populateWithJSON:(NSDictionary*)json inContext:(NSManagedObjectContext*)context;

- (BOOL)containsSongs;

- (NSString*)setlistAsString;

- (NSString*)dateDisplayString;

- (NSArray*)arrayOfAllSongs;

- (BOOL)allSongsCheckedForSpotifyURI;

@end
