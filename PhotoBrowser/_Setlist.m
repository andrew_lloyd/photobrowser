// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Setlist.m instead.

#import "_Setlist.h"

const struct SetlistAttributes SetlistAttributes = {
	.artistName = @"artistName",
	.date = @"date",
	.setlistURL = @"setlistURL",
};

const struct SetlistRelationships SetlistRelationships = {
	.containsSets = @"containsSets",
	.inVenue = @"inVenue",
};

@implementation SetlistID
@end

@implementation _Setlist

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Setlist" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Setlist";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Setlist" inManagedObjectContext:moc_];
}

- (SetlistID*)objectID {
	return (SetlistID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic artistName;

@dynamic date;

@dynamic setlistURL;

@dynamic containsSets;

- (NSMutableOrderedSet*)containsSetsSet {
	[self willAccessValueForKey:@"containsSets"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"containsSets"];

	[self didAccessValueForKey:@"containsSets"];
	return result;
}

@dynamic inVenue;

@end

@implementation _Setlist (ContainsSetsCoreDataGeneratedAccessors)
- (void)addContainsSets:(NSOrderedSet*)value_ {
	[self.containsSetsSet unionOrderedSet:value_];
}
- (void)removeContainsSets:(NSOrderedSet*)value_ {
	[self.containsSetsSet minusOrderedSet:value_];
}
- (void)addContainsSetsObject:(Set*)value_ {
	[self.containsSetsSet addObject:value_];
}
- (void)removeContainsSetsObject:(Set*)value_ {
	[self.containsSetsSet removeObject:value_];
}
- (void)insertObject:(Set*)value inContainsSetsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"containsSets"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self containsSets]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"containsSets"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"containsSets"];
}
- (void)removeObjectFromContainsSetsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"containsSets"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self containsSets]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"containsSets"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"containsSets"];
}
- (void)insertContainsSets:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"containsSets"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self containsSets]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"containsSets"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"containsSets"];
}
- (void)removeContainsSetsAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"containsSets"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self containsSets]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"containsSets"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"containsSets"];
}
- (void)replaceObjectInContainsSetsAtIndex:(NSUInteger)idx withObject:(Set*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"containsSets"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self containsSets]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"containsSets"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"containsSets"];
}
- (void)replaceContainsSetsAtIndexes:(NSIndexSet *)indexes withContainsSets:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"containsSets"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self containsSets]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"containsSets"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"containsSets"];
}
@end

