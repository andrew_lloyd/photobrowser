//
//  PhotoBrowserAppDelegate.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 06/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import "PhotoBrowserAppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <Spotify/Spotify.h>
#import "Config.h"
#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>
#import "RKObjectManager.h"
#import "Setlist.h"

@interface PhotoBrowserAppDelegate ()


@end

@implementation PhotoBrowserAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Set up shared authentication information
    SPTAuth *auth = [SPTAuth defaultInstance];
    auth.clientID = @kClientId;
    auth.requestedScopes = @[SPTAuthStreamingScope, SPTAuthPlaylistModifyPublicScope, SPTAuthPlaylistReadPrivateScope, SPTAuthPlaylistReadPrivateScope];
    auth.redirectURL = [NSURL URLWithString:@kCallbackURL];
#ifdef kTokenSwapServiceURL
    auth.tokenSwapURL = [NSURL URLWithString:@kTokenSwapServiceURL];
#endif
#ifdef kTokenRefreshServiceURL
    auth.tokenRefreshURL = [NSURL URLWithString:@kTokenRefreshServiceURL];
#endif
    auth.sessionUserDefaultsKey = @kSessionUserDefaultsKey;
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    
        // Check for iOS 7+
        UINavigationBar *aNavBar = [[UINavigationBar alloc]init];
        if ([aNavBar respondsToSelector:@selector(setBarTintColor:)])
        {
            [[UINavigationBar appearance]setBarTintColor:[UIColor colorWithRed:252.0 / 255.0 green:224.0 / 255.0 blue:11.0 / 255.0 alpha:1]];
            [[UINavigationBar appearance]setTintColor:[UIColor whiteColor]];
            [[UINavigationBar appearance]setAlpha:0.3];

            [[UISegmentedControl appearance] setTintColor:[UIColor blackColor]];
            [[UISegmentedControl appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName:[UIColor whiteColor] } forState:UIControlStateSelected];
            
        
            
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    SPTAuth *auth = [SPTAuth defaultInstance];
    
    SPTAuthCallback authCallback = ^(NSError *error, SPTSession *session) {
        // This is the callback that'll be triggered when auth is completed (or fails).
        
        if (error != nil) {
            NSLog(@"*** Auth error: %@", error);
            return;
        }
        
        auth.session = session;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sessionUpdated" object:self];
    };
    
    /*
     Handle the callback from the authentication service. -[SPAuth -canHandleURL:]
     helps us filter out URLs that aren't authentication URLs (i.e., URLs you use elsewhere in your application).
     */
    
    if ([auth canHandleURL:url]) {
        [auth handleAuthCallbackWithTriggeredAuthURL:url callback:authCallback];
        return YES;
    }
    
    return NO;
}

@end
