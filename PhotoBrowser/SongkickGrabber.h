//
//  SongkickGrabber.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 20/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//
#import <Foundation/Foundation.h>


@interface SongkickGrabber : NSObject

@property(nonatomic, strong)NSString *venueID;
@property(nonatomic, strong)NSArray *events;
@property(nonatomic, strong)NSString *eventName;
@property(nonatomic, strong)NSArray *eventArtists;

- (void)fetchEvent:(NSArray*)placeNames onDate:(NSDate *)theDate;
-(NSString*)getEvent;
-(NSArray*)getArtists;

@end