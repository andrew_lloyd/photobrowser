//
//  HomeViewController.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 19/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end
