//
//  PlaylistAddedConfirmationViewController.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 21/10/2015.
//  Copyright © 2015 Andrew Lloyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Setlist.h"

@interface PlaylistAddedConfirmationViewController : UIViewController

@property(strong, nonatomic) Setlist *setlist;

@property (weak, nonatomic) IBOutlet UIView *confirmationView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UIView *backdropView;

@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;

- (IBAction)closePressed:(id)sender;

- (void)showInView:(UIView *)aView animated:(BOOL)animated;

@end
