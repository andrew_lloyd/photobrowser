//
//  PlaylistAddedConfirmationViewController.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 21/10/2015.
//  Copyright © 2015 Andrew Lloyd. All rights reserved.
//

#import "PlaylistAddedConfirmationViewController.h"
#import "SpotifyServices.h"

@interface PlaylistAddedConfirmationViewController ()

@end

@implementation PlaylistAddedConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self roundPopupViewCorners];
    
    self.confirmationView.hidden = YES;
    [self createPlaylist];
}

- (void)createPlaylist
{
    self.spinner.hidesWhenStopped = YES;
    self.spinner.hidden = NO;
    [self.spinner startAnimating];
    
    SpotifyServices *spotifySer = [[SpotifyServices alloc] init];
    [spotifySer createPlaylistForSetWithSetlist:_setlist
                                   withSuccess:^{
                                       self.confirmationView.hidden = NO;
                                       [self.spinner stopAnimating];
                                       
                                       [self performSelector:@selector(closePressed:) withObject:nil afterDelay:2.0];

                                   } andFailure:^(NSError *error) {
                                       UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"There has been a problem adding the playlist."
                                                                                                      message:error.localizedDescription
                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                       
                                       UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                                             handler:^(UIAlertAction * action) {
                                                                                                 [self.spinner stopAnimating];
                                                                                                 
                                                                                                 [self performSelector:@selector(closePressed:) withObject:nil afterDelay:0.0];
                                                                                             }];
                                       
                                       [alert addAction:defaultAction];
                                       [self presentViewController:alert animated:YES completion:nil];
                                   }];
}

- (void)roundPopupViewCorners
{
    //round corners of check box
    // Create the path
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.popupView.bounds
                                                   byRoundingCorners:UIRectCornerAllCorners
                                                         cornerRadii:CGSizeMake(5.0, 5.0)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.popupView.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    self.popupView.layer.mask = maskLayer;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closePressed:(id)sender
{
    [self closeView];
}

- (void)showInView:(UIView *)aView animated:(BOOL)animated
{
    [aView addSubview:self.view];
    if (animated) {
        [self showAnimate];
    }
}

- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
         self.backdropView.alpha = 0.2;
        self.popupView.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)closeView
{
    [UIView animateWithDuration:.35 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}

@end
