//
//  PhotoBrowserViewController.m
//  PhotoBrowser
///Users/andrewlloyd/PhotoBrowser/cloned/PhotoBrowser/PhotoBrowserViewController.m
//  Created by Andrew Lloyd on 06/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import "ImageViewController.h"
#import "PhotoBrowserViewController.h"
#import "PlaceGrabber.h"
#import "FlickrSearchViewController.h"
#import "SetlistViewController.h"
#import "ReviewViewController.h"


@import MobileCoreServices;

@interface PhotoBrowserViewController () 


@property (nonatomic) UIImagePickerController *imagePickerController;
@property (nonatomic) CLLocationManager *locationManager;

@property (nonatomic, strong) NSString *tempDataStore;
@property (nonatomic, strong) NSArray *thePlaceNames;
@property (nonatomic, strong) NSString *theEvent;
@property (nonatomic, strong) NSArray *theSet;



@end


@implementation PhotoBrowserViewController 

@synthesize locationManager, image, imageButton, facebookButton, tweetButton, reviewsButton, storedPhotoLocation, datePhotoWasTaken, thePlaceNames, theEvent, artistLabel, venueLabel, dateLabel, browseOthersButton, downloadingSpinner, theArtists, bandPicker, theSet, viewThisSetlist;

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    //set nav bar properties
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:0 green:164.0/255.0 blue:180.0/255/.0 alpha:1.0]};
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0 green:184.0/255.0 blue:1 alpha:1.0];
    
    //hide download spinner as we dont need it yet
    [downloadingSpinner setHidden:YES];
    
	// Do any additional setup after loading the view, typically from a nib.
    browseOthersButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    browseOthersButton.titleLabel.numberOfLines = 3;
    browseOthersButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.selectedImages = [[NSMutableArray alloc] init];
    
    bandPicker.delegate = self;
    
    //device does not have a camera, show error warning.
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
    
    BOOL locationAllowed = [CLLocationManager locationServicesEnabled];
    
    //check if location services are enabled
    if(locationAllowed){
        
        NSLog(@"Location Services Enabled");
        
        self.locationManager = [[CLLocationManager alloc] init];
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.delegate = self;
        [locationManager startUpdatingLocation];
    }
    
    if(!locationAllowed){
        NSLog(@"Locations switched off");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled"
                                                        message:@"To re-enable, please go to Settings and allow Location Services for this app."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    [viewThisSetlist setEnabled:NO];
    [imageButton setEnabled:NO];
    [facebookButton setEnabled:NO];
    [tweetButton setEnabled:NO];
    [reviewsButton setEnabled:NO];
    [browseOthersButton setEnabled:NO];
    [browseOthersButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [browseOthersButton setTitleColor:[UIColor colorWithRed:235.0/255.0 green:16.0/255.0 blue:89.0/255.0 alpha:1] forState:UIControlStateNormal];
    [reviewsButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [reviewsButton setTitleColor:[UIColor colorWithRed:235.0/255.0 green:16.0/255.0 blue:89.0/255.0 alpha:1] forState:UIControlStateNormal];
    
    
    [[imageButton layer] setBorderWidth:2.0f];
    [[imageButton layer] setBorderColor:[UIColor blackColor].CGColor];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIImage *)getImage
{
    UIImage *thePhoto = [self.selectedImages objectAtIndex:0];
    return thePhoto;
}

- (IBAction)takePhoto:(UIButton *)sender
{
    
    BOOL locationAllowed = [CLLocationManager locationServicesEnabled];
    if(locationAllowed){
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Device has no camera"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
            
            [myAlertView show];
        }else{
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
        }
    }else{
        NSLog(@"Theyre trying to take a photo with locations switched off");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled"
                                                        message:@"This feature requires Location Services. To re-enable, please go to Settings and turn on Location Service for this app."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)prepareImageViewController:(ImageViewController *)ivc toDisplayPhoto:(NSMutableArray *)photo
{
    ivc.image = [photo objectAtIndex:0];
    ivc.title = theEvent;
}


- (IBAction)selectPhoto:(UIButton *)sender
{
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (IBAction)enlargeImage:(UIButton *)sender {

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Display Photo"])
    {
        if ([segue.destinationViewController isKindOfClass:[ImageViewController class]])
        {
            [self prepareImageViewController:segue.destinationViewController toDisplayPhoto:self.selectedImages];
        }
    }else if ([segue.identifier isEqualToString:@"Browse Others"])
    {
        [self prepareBrowseOthersViewController:segue.destinationViewController];
    }else if ([segue.identifier isEqualToString:@"viewThisSetlist"])
    {
        [self prepareSetlistViewController:segue.destinationViewController];
    }else if([segue.identifier isEqualToString:@"getReviews"])
    {
        [self prepareReviewViewController:segue.destinationViewController];
    }
}

- (void)prepareSetlistViewController:(SetlistViewController *)svc
{
    svc.artist = theEvent;
    svc.venue = [thePlaceNames firstObject];
    svc.dateOfShow = datePhotoWasTaken;
    
    svc.title = [NSString stringWithFormat:@"Setlist for %@", theEvent];
}

- (void)prepareBrowseOthersViewController:(FlickrResultsTableViewController *)tvc
{
    NSString *query = [NSString stringWithFormat:@"%@ %@", theEvent, [thePlaceNames firstObject]];
    query = [query stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    tvc.query = query;
    tvc.dateTaken = datePhotoWasTaken;
    tvc.title = [NSString stringWithFormat:@"Results for %@", theEvent];
}

-(void)prepareReviewViewController:(ReviewViewController *)rvc
{
    NSString *strVenue = [thePlaceNames firstObject];
    
    NSString *urlString = [NSString stringWithFormat:@"https://www.google.co.uk/search?hl=en&q=%@+%@+review", theEvent, strVenue];
                       
    NSURL *theURL = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    rvc.theURLquery = theURL;
    rvc.title = [NSString stringWithFormat:@"Reviews for %@", theEvent];
}


- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    if (self.selectedImages.count > 0 && sourceType == UIImagePickerControllerSourceTypePhotoLibrary)
    {
        [self.selectedImages removeAllObjects];
    }
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = NO;
    }
    
    self.imagePickerController = imagePickerController;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
    
}

- (void)finishAndUpdate
{
    
    if ([self.selectedImages count] > 0)
    {
        if ([self.selectedImages count] == 1)
        {
            // Camera took a single picture.
            UIImage *thePhoto = [self.selectedImages objectAtIndex:0];
            
            [[self imageButton]setTitle:@"" forState:UIControlStateNormal];
            
            //stretches the image, ***!! Needs Fixing!!***
            [[self imageButton]setBackgroundImage:thePhoto forState:UIControlStateNormal];
            
            //change text view
            NSLog(@"Done, ya wee photo.");
            
            if ([theEvent isEqualToString:@"Unrecognized Venue"] || [theEvent isEqualToString:@"Unknown Event"]) {
                //keeps functions disabled
                [imageButton setEnabled:YES];
                [facebookButton setEnabled:NO];
                [tweetButton setEnabled:NO];
                [browseOthersButton setEnabled:NO];
                [browseOthersButton setTintColor:[UIColor grayColor]];
                [viewThisSetlist setEnabled:NO];
                [reviewsButton setEnabled:NO];
            }else{
                //enables functions only available with a photo selected and with a recognized venue
                [imageButton setEnabled:YES];
                [facebookButton setEnabled:YES];
                [tweetButton setEnabled:YES];
                [browseOthersButton setEnabled:YES];
                [viewThisSetlist setEnabled:YES];
                [reviewsButton setEnabled:YES];
            }
        }
    }
    
    [downloadingSpinner stopAnimating];
    [downloadingSpinner setHidden:YES];
    
    self.imagePickerController = nil;
}

#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // To be ready to start again, clear the captured images array.
    [self.selectedImages removeAllObjects];
    
    //set labels for loading
    [dateLabel setText:@""];
    [venueLabel setText:@"Loading..."];
    [artistLabel setText:@""];
    
    image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    //turn on network activity indicator
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [downloadingSpinner setHidden:NO];
    [downloadingSpinner startAnimating];

    [self.selectedImages addObject:image];
    
    //save photo to camera roll.
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        [self saveImage:image withInfo:info];
//        NSDictionary *metadata = [info objectForKey:UIImagePickerControllerMediaMetadata];
//        NSLog(@"%@ %@", metadata, info);
        storedPhotoLocation = [locationManager location];
        NSLog(@"%@", storedPhotoLocation);
        
        //getDate
        datePhotoWasTaken = [NSDate date];
        NSLog(@"Photo was created: %@", datePhotoWasTaken);
        
        PlaceGrabber *pg = [[PlaceGrabber alloc]init];
        [pg setPhotoLatitudeAndLongitude:storedPhotoLocation];
        [pg fetchPlace:datePhotoWasTaken];
        
        thePlaceNames = [pg getPlaces];
        theEvent = [pg getTheEvent];
        theArtists = [pg getArtists];
        
        if ([theEvent isEqualToString:@"Unrecognized Venue"]) {
            UIAlertView *unknownVenueAlert = [[UIAlertView alloc] initWithTitle:@"Unrecognized Venue"
                                                            message:@"Sorry, we don't recognise the location of this photograph as a registered venue."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [unknownVenueAlert show];
            [artistLabel setText:@""];
            [venueLabel setText:@"Unrecognized Venue"];
            [bandPicker setHidden:YES];
        }
        
        if ([theEvent isEqualToString:@"Unknown Event"]) {
            
            UIAlertView *unknownEventAlert=[[UIAlertView alloc]initWithTitle:@"Unknown Event" message:@"We don't have an event registered for that venue on this date. Would you like to add one?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
            unknownEventAlert.alertViewStyle=UIAlertViewStylePlainTextInput;
    
            [unknownEventAlert show];
            [bandPicker setHidden:YES];
        }
        

            [bandPicker setHidden:NO];
            [bandPicker reloadAllComponents];
        
        
        [self finishAndUpdate];
        
        [self setLabelsWithArtistString:theEvent andVenue:[thePlaceNames firstObject] andTheDate:datePhotoWasTaken];

    }
    else {
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        if ([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
            NSURL *url = [info objectForKey:UIImagePickerControllerReferenceURL];
            if (url) {
                ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset) {
                    CLLocation *location = [myasset valueForProperty:ALAssetPropertyLocation];
                    // location contains lat/long, timestamp, etc
                    // extracting the image is more tricky and 5.x beta ALAssetRepresentation has bugs!
                    NSLog(@"%@", location);
                    storedPhotoLocation = location;
                    
                    //getDate
                    datePhotoWasTaken = [myasset valueForProperty:ALAssetPropertyDate];
                    NSLog(@"Photo was created: %@", datePhotoWasTaken);
                    
                    PlaceGrabber *pg = [[PlaceGrabber alloc]init];
                    [pg setPhotoLatitudeAndLongitude:storedPhotoLocation];
                    [pg fetchPlace:datePhotoWasTaken];
                    
                    thePlaceNames = [pg getPlaces];
                    theEvent = [pg getTheEvent];
                    theArtists = [pg getArtists];
                    
                    if ([theEvent isEqualToString:@"Unrecognized Venue"]) {
                        UIAlertView *unknownVenueAlert = [[UIAlertView alloc] initWithTitle:@"Unrecognized Venue"
                                                                                    message:@"Sorry, we don't recognise the location of this photograph as a registered venue."
                                                                                   delegate:nil
                                                                          cancelButtonTitle:@"OK"
                                                                          otherButtonTitles:nil];
                        [unknownVenueAlert show];
                        [bandPicker setHidden:YES];
                    }
                    
                    if ([theEvent isEqualToString:@"Unknown Event"]) {
                        
                        UIAlertView *unknownEventAlert=[[UIAlertView alloc]initWithTitle:@"Unknown Event" message:@"We don't have an event registered for that venue on this date. Would you like to declare one?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                        unknownEventAlert.alertViewStyle=UIAlertViewStylePlainTextInput;
                        UITextField * alertTextField = [unknownEventAlert textFieldAtIndex:0];
                        alertTextField.placeholder = @"Enter the artist";
                        [unknownEventAlert show];
                        
                        [bandPicker setHidden:YES];
                    }
                    

                    NSLog(@"2 artists played");
                    [bandPicker setHidden:NO];
                    [bandPicker reloadAllComponents];

                    
                    
                    [self setLabelsWithArtistString:theEvent andVenue:[thePlaceNames firstObject] andTheDate:datePhotoWasTaken];
                    [self finishAndUpdate];
                    
                    
                };
                ALAssetsLibraryAccessFailureBlock failureblock = ^(NSError *myerror) {
                    NSLog(@"cant get image - %@", [myerror localizedDescription]);
                };
                ALAssetsLibrary *assetsLib = [[ALAssetsLibrary alloc] init];
                [assetsLib assetForURL:url resultBlock:resultblock failureBlock:failureblock];
            }
        }
    }
    //turn off network activity indicator
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSLog(@"Button Index =%ld",(long)buttonIndex);
    if (buttonIndex == 1) {  //Login
        UITextField *input = [alertView textFieldAtIndex:0];
        NSLog(@"input: %@", input.text);
        theEvent = input.text;
        artistLabel.text = theEvent;
        if (![theEvent isEqualToString:@""])
        {
            [imageButton setEnabled:YES];
            [facebookButton setEnabled:YES];
            [tweetButton setEnabled:YES];
            [browseOthersButton setEnabled:YES];
            [viewThisSetlist setEnabled:YES];
            [reviewsButton setEnabled:YES];
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //do nothing extra
    //shows warning without it
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

//Some code from saveImage taken from https://github.com/5teev/MetaPhotoSave
- (void) saveImage:(UIImage *)imageToSave withInfo:(NSDictionary *)info
{
    // Get the image metadata (EXIF & TIFF)
    NSMutableDictionary * imageMetadata = [[info objectForKey:UIImagePickerControllerMediaMetadata] mutableCopy];
    NSDictionary *locationDictionary = [self gpsDictionaryForLocation:locationManager];
    NSLog(@"%@", locationDictionary);
    
    //creates imagemetadata with gpsdictionaryforlocation method using locationManager
    [imageMetadata setObject:locationDictionary forKey:(NSString*)kCGImagePropertyGPSDictionary];
    
    
    // Get the assets library
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    // create a completion block for when we process the image
    ALAssetsLibraryWriteImageCompletionBlock imageWriteCompletionBlock =
    ^(NSURL *newURL, NSError *error) {
        if (error) {
            NSLog( @"Error writing image with metadata to Photo Library: %@", error );
        } else {
            NSLog( @"Wrote image %@ with metadata %@ to Photo Library",newURL,imageMetadata);
        }
    };
    
    // Save the new image to the Camera Roll, using the completion block defined just above
    [library writeImageToSavedPhotosAlbum:[imageToSave CGImage]
                                 metadata:imageMetadata
                          completionBlock:imageWriteCompletionBlock];
}

/**
 Some code from gpsDictionaryForLocation taken from https://github.com/5teev/MetaPhotoSave
 
 A convenience method to generate the {GPS} portion of a photo's EXIF data from a CLLLocation.
 
 @param location the location to base the NSDictionary on
 
 @return NSDictionary containing {GPS} block for a photo's EXIF data
 */
- (NSDictionary *) gpsDictionaryForLocation:(CLLocationManager *)locationMan
{
    //get current latitude from location manager
    CLLocationDegrees exifLatitude  = locationMan.location.coordinate.latitude;
    //get current longitude from location manager
    CLLocationDegrees exifLongitude = locationMan.location.coordinate.longitude;
    
    //set latitude ref tags in gps dictionary
    NSString * latRef;
    NSString * longRef;
    if (exifLatitude < 0.0) {
        exifLatitude = exifLatitude * -1.0f;
        latRef = @"S";
    } else {
        latRef = @"N";
    }
    
    if (exifLongitude < 0.0) {
        exifLongitude = exifLongitude * -1.0f;
        longRef = @"W";
    } else {
        longRef = @"E";
    }
    
    
    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
    
    // requires ImageIO
    [locDict setObject:locationMan.location.timestamp forKey:(NSString*)kCGImagePropertyGPSTimeStamp];
    [locDict setObject:latRef forKey:(NSString*)kCGImagePropertyGPSLatitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLatitude] forKey:(NSString *)kCGImagePropertyGPSLatitude];
    [locDict setObject:longRef forKey:(NSString*)kCGImagePropertyGPSLongitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLongitude] forKey:(NSString *)kCGImagePropertyGPSLongitude];
    [locDict setObject:[NSNumber numberWithFloat:locationMan.location.horizontalAccuracy] forKey:(NSString*)kCGImagePropertyGPSDOP];
    [locDict setObject:[NSNumber numberWithFloat:locationMan.location.altitude] forKey:(NSString*)kCGImagePropertyGPSAltitude];
    
    return locDict;
    
}

#pragma mark - social sharing

- (IBAction)tweet:(id)sender {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        if([self.selectedImages objectAtIndex:0])
        {
            id test = [thePlaceNames firstObject];
            
            //remove spaces and punctuation as twitter hashtags dont allow these
            //edited code found at  http://stackoverflow.com/questions/1231764/nsstring-convert-to-pure-alphabet-only-i-e-remove-accentspunctuation?answertab=votes#tab-top
            NSString *venueSpacesRemoved = [test stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *finishedVenueString = [[venueSpacesRemoved componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]]componentsJoinedByString:@""];
            
            NSString *eventSpacesRemoved = [theEvent stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *finishedEventString = [[eventSpacesRemoved componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]]componentsJoinedByString:@""];
            
            SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet setInitialText:[NSString stringWithFormat:@"I saw #%@ at  #%@. Shared via #ShowSnapper", finishedEventString, finishedVenueString]];
            
            //add photo
            [tweetSheet addImage:[self.selectedImages objectAtIndex:0]];
            
            //add link
            //[tweetSheet addURL:[NSURL URLWithString:@"www.google.com"]];
            [self presentViewController:tweetSheet animated:YES completion:nil];
        }
    }
}


- (IBAction)facebookPost:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        if([self.selectedImages objectAtIndex:0])
        {
            SLComposeViewController *facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            
            id theVenueName = [thePlaceNames firstObject];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"EEEE, dd MMMM yyyy"];
            NSString *dayString = [dateFormatter stringFromDate:datePhotoWasTaken];
            
            [facebookSheet setInitialText:[NSString stringWithFormat:@"I saw %@ at %@. %@. (Shared via ShowSnapper)", theEvent, theVenueName, dayString]];
            
            //add photo
            [facebookSheet addImage:[self.selectedImages objectAtIndex:0]];
            
            [self presentViewController:facebookSheet animated:YES completion:nil];
        }
    }
}

-(void)setLabelsWithArtistString:(NSString*)artist andVenue:(NSString*)venue andTheDate:(NSDate *)date
{
    if([self.theArtists isKindOfClass:[NSArray class]] || [self.theArtists isKindOfClass:[NSString class]])
    {
            artistLabel.text = @"";
    }
    else{
        artistLabel.text = theEvent;
    }

    venueLabel.text = venue;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEEE, dd MMMM yyyy"];
    NSString *dayString = [dateFormatter stringFromDate:datePhotoWasTaken];
    
    dateLabel.text = dayString;
}

-(NSArray* )getArtists
{
    return theArtists;
}

#pragma mark - picker view delegate
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    if([self.theArtists isKindOfClass:[NSString class]])
        {
            return 1;
        }else{
            return [self.theArtists count];
        }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    if([self.theArtists isKindOfClass:[NSString class]])
        {
            return (NSString*) theArtists;
        }else{
            return [self.theArtists objectAtIndex:row];
        }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    if([self.theArtists isKindOfClass:[NSString class]])
    {
        theEvent = (NSString*)theArtists;
    }else{
        NSLog(@"Chosen artists: %@", [theArtists objectAtIndex:row]);
        theEvent = [theArtists objectAtIndex:row];
    }
}


//code to change picker font from http://stackoverflow.com/questions/20698547/how-can-i-change-text-font-in-picker-in-ios-7
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* lbl = (UILabel*)view;
    // Customise Font
    if (lbl == nil) {
        //label size
        //CGRect frame = CGRectMake(0.0, 0.0, 70, 30);
        
        lbl = [[UILabel alloc] init];
        
        [lbl setTextAlignment:NSTextAlignmentCenter];
        
        [lbl setBackgroundColor:[UIColor clearColor]];
        //here you can play with fonts
        [lbl setFont:[UIFont fontWithName:@"American Typewriter" size:20.0]];
        
    }
    //picker view array is the datasource
    if([self.theArtists isKindOfClass:[NSString class]])
    {
        [lbl setText:(NSString*)[self theArtists]];
    }else{
        [lbl setText:[theArtists objectAtIndex:row]];
    }
    
    return lbl;
}


@end
