//
//  DateComparison.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 28/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//


#import "NSDate+SameDay.h"

//code from http://stackoverflow.com/questions/1561554/cocoa-touch-how-do-i-see-if-two-nsdates-are-in-the-same-day


@implementation NSDate (SameDay)


- (BOOL)isSameDayAsDate:(NSDate*)otherDate {
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:self];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:otherDate];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}


@end
