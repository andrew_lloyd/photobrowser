//
//  PlaceGrabber.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 20/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import "PlaceGrabber.h"
#import "GooglePlacesAPIKey.h"
#import "SongkickGrabber.h"

@interface PlaceGrabber()

@property (nonatomic) CLLocationDegrees latitude;
@property (nonatomic) CLLocationDegrees longitude;

@end

@implementation PlaceGrabber

@synthesize latitude, longitude, places, eventName, eventArtists;

-(void)setPhotoLatitudeAndLongitude:(CLLocation *)location
{
    CLLocationCoordinate2D coord = location.coordinate;
    self.latitude = coord.latitude;
    self.longitude = coord.longitude;
}

- (NSURL *)URLForPlaceWithLatitude:(CLLocationDegrees)lat andLongitude:(CLLocationDegrees)lon
{
    NSString *latStr = [NSString stringWithFormat:@"%.6f", lat];
    NSString *lonStr = [NSString stringWithFormat:@"%.6f", lon];
    
    //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=51.503473,-0.224177&radius=5&types=bar&sensor=false&key=AIzaSyBIWlTXB9nErPbIiiRWPFotYZdNGx-Db2E
    
    NSString * urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%@,%@&types=establishment&radius=30&sensor=false&key=%@", latStr, lonStr, GoogleAPIKey];
    return [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}

- (void)fetchPlace:(NSDate *)dateTaken;
{
    NSURL *url = [self URLForPlaceWithLatitude:latitude andLongitude:longitude]; //get the URL with query
    
    // Threading not working!!
    //dispatch_queue_t fetchQ = dispatch_queue_create("venueName grabber", NULL);
    // put a block to do the fetch onto that queue
    //dispatch_async(fetchQ, ^{
        // fetch the JSON data from Google
        NSData *jsonResults = [NSData dataWithContentsOfURL:url];
        // convert it to a Property List (NSArray and NSDictionary)
        NSDictionary *propertyListResults = [NSJSONSerialization JSONObjectWithData:jsonResults
                                                                            options:0
                                                                              error:NULL];
        NSLog(@"%@", propertyListResults);
    
        // get the NSArray of place NSDictionarys out of the results
        places = [propertyListResults valueForKeyPath:GOOGLE_RESULTS_PLACES];
        self.places = places;
        NSLog(@"%@", places);
    
        if (places.count == 0) {
            NSLog(@"No results");
            
            eventName = @"Unrecognized Venue";
        }else{
            SongkickGrabber *sg = [[SongkickGrabber alloc]init];
            [sg fetchEvent:places onDate:dateTaken];
    
            eventName = [sg getEvent];
            eventArtists = [sg getArtists];
        }
}

-(NSArray *)getPlaces
{
    return places;
}

-(NSString *)getTheEvent
{
    return eventName;
}

-(NSArray *)getArtists
{
    return eventArtists;
}

@end
