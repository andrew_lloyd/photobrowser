//
//  ImageViewController.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 17/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController () <UIScrollViewDelegate, UISplitViewControllerDelegate>

@property(weak, nonatomic) IBOutlet UIActivityIndicatorView *spinIndicator;
@property(nonatomic, strong) IBOutlet UIScrollView *scrollerView;
@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation ImageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.scrollerView addSubview:self.imageView];
}

- (UIImageView *)imageView
{
    if (!_imageView) _imageView = [[UIImageView alloc] init];
    return _imageView;
}

- (UIImage *)image
{
    return self.imageView.image;
}

- (void)setImage:(UIImage *)image
{
    [self.spinIndicator startAnimating];
    self.imageView.image = image;
    
    self.scrollerView.zoomScale = 1.0;
    self.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);

    self.scrollerView.contentSize = self.image ? self.image.size : CGSizeZero;
}

- (void)setScrollView:(UIScrollView *)scrollView
{
    _scrollerView = scrollView;
    
    // sets zooming scales
    _scrollerView.minimumZoomScale = 0.2;
    _scrollerView.maximumZoomScale = 1.5;
    _scrollerView.delegate = self;
    
    self.scrollerView.contentSize = self.image ? self.image.size : CGSizeZero;
    
    [self.spinIndicator stopAnimating];
    [self.spinIndicator setHidden:YES];
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

#pragma mark - Setting the Image from the Image's URL

- (void)setImageURL:(NSURL *)imageURL
{
    _imageURL = imageURL;
    [self startDownloadingImage];
}

//some of this code is taken from the Stanford tutorial
- (void)startDownloadingImage
{
    self.image = nil;
    
    if (self.imageURL)
    {
        [self.spinIndicator startAnimating];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:self.imageURL];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        
        NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request
                                                        completionHandler:^(NSURL *localfile, NSURLResponse *response, NSError *error) {
                                                            if (!error) {
                                                                if ([request.URL isEqual:self.imageURL]) {
                                                                    // Use UIImage because you cant change UI on thread
                                                                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:localfile]];
                                                                    //go back to main thread to set the fetched image to 'self.image'
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        self.image = image;
                                                                        [self.spinIndicator stopAnimating];
                                                                        [self.spinIndicator setHidden:YES];
                                                                    });
                                                                }
                                                            }
                                                        }];
        [task resume]; // don't forget that all NSURLSession tasks start out suspended!
    }
}


#pragma mark - UISplitViewControllerDelegate

//following methods taken from Stanford Tutorial
- (void)awakeFromNib
{
    self.splitViewController.delegate = self;
}

- (BOOL)splitViewController:(UISplitViewController *)svc
   shouldHideViewController:(UIViewController *)vc
              inOrientation:(UIInterfaceOrientation)orientation
{
    return UIInterfaceOrientationIsPortrait(orientation);
}

- (void)splitViewController:(UISplitViewController *)svc
     willHideViewController:(UIViewController *)aViewController
          withBarButtonItem:(UIBarButtonItem *)barButtonItem
       forPopoverController:(UIPopoverController *)pc
{
    barButtonItem.title = aViewController.title;
    self.navigationItem.leftBarButtonItem = barButtonItem;
}

- (void)splitViewController:(UISplitViewController *)svc
     willShowViewController:(UIViewController *)aViewController
  invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    self.navigationItem.leftBarButtonItem = nil;
}



@end
