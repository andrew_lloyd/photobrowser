//
//  PhotoBrowserAppDelegate.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 06/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoBrowserAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
