//
//  SetlistGrabber.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 31/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import "SetlistGrabber.h"
#import "Setlist.h"
#import <CoreData/CoreData.h>


#import "PhotoBrowserAppDelegate.h"

@implementation SetlistGrabber

@synthesize setlists, setlistSongs, returnedSetlistDate, returendSetlistVenue;

-(void)fetchSetlistofArtist:(NSString*)artistName atVenue:(NSString*)venueName onDate:(NSString*)date inContext:(NSManagedObjectContext*)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Setlist"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:nil];
    
    for (Setlist *set in fetchedObjects)
    {
        [context deleteObject:set];
    }
    
    NSString *query = artistName;
    NSString *venueQuery = venueName;

    NSString *urlString = [NSString stringWithFormat:@"http://api.setlist.fm/rest/0.1/search/setlists.json?artistName=%@&venueName=%@&date=%@", query, venueQuery, date, nil];
    
    NSURL *theURL = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    
    // fetch the JSON data from Setlist.fm
    NSMutableString *jsonResults = [NSMutableString stringWithContentsOfURL:theURL encoding:NSUTF8StringEncoding error:nil];
    
    NSMutableString *jsonReslutsFixed = jsonResults;
    NSRange wholeThing = NSMakeRange(0, [jsonReslutsFixed length]);
    [jsonReslutsFixed replaceOccurrencesOfString:@"@" withString:@"" options:1 range:wholeThing];
    
    NSData *jsonResultsMended = [jsonReslutsFixed  dataUsingEncoding:NSUTF8StringEncoding];
    
    if (jsonResultsMended != nil) {
        
        // convert it to a Property List (NSArray and NSDictionary)
        NSDictionary *setlistResults = [NSJSONSerialization JSONObjectWithData:jsonResultsMended
                                                                  options:0
                                                                    error:NULL];
        
        setlists = [setlistResults valueForKeyPath:@"setlists.setlist"];
    
        for (NSDictionary *setlistJSON in setlists)
        {
            if ([setlistJSON isKindOfClass:[NSString class]])
            {
                //handle results if only one setlist is returned
                Setlist *setlist = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Setlist"
                                    inManagedObjectContext:context];
                
                [setlist populateWithJSON:setlists inContext:context];
                break;
            }
            else
            {
                Setlist *setlist = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Setlist"
                                    inManagedObjectContext:context];
                
                [setlist populateWithJSON:setlistJSON inContext:context];
            }
        }
    }
}


@end
