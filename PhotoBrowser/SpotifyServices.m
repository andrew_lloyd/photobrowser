//
//  SpotifyServices.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 21/10/2015.
//  Copyright © 2015 Andrew Lloyd. All rights reserved.
//

#import "SpotifyServices.h"
#import "Venue.h"
#import "Song.h"

@implementation SpotifyServices

-(void)createPlaylistForSetWithSetlist:(Setlist*)setlist
                              withSuccess:(void (^) ())success
                           andFailure:(void (^) (NSError *error))failure
{
    SPTAuth *auth = [SPTAuth defaultInstance];
    
    [SPTUser requestCurrentUserWithAccessToken:auth.session.accessToken callback:^(NSError *error, id object) {
        if (error != nil) {
            NSLog(@"*** Failed to get playlist %@", error);
            failure(error);
            return;
        }
        else
        {
            SPTUser *currentUser = object;
            
            NSString *playlistName = [NSString stringWithFormat:@"%@ at %@. %@.", setlist.artistName, setlist.inVenue.name,[setlist dateDisplayString]];
            [SPTPlaylistList createPlaylistWithName:playlistName forUser:currentUser.canonicalUserName publicFlag:YES accessToken:auth.session.accessToken callback:^(NSError *error, SPTPlaylistSnapshot *playlist)
             {
                 if (error != nil) {
                     NSLog(@"*** Failed to get playlist %@", error);
                     failure(error);
                     return;
                 }
                 else
                 {
                     NSArray *songArray = [setlist arrayOfAllSongs];
                     for (Song *songTrack in songArray)
                     {
                         NSString *searchQuery = [NSString stringWithFormat:@"%@ %@", setlist.artistName, songTrack.name];
                         [SPTSearch performSearchWithQuery:searchQuery queryType:SPTQueryTypeTrack accessToken:auth.session.accessToken callback:^(NSError *error, id object) {
                             if (error != nil) {
                                 NSLog(@"*** Failed to get playlist %@", error);
                                 failure(error);
                                 return;
                             }
                             else if(object)
                             {
                                 SPTListPage *listPage = object;
                                 NSArray *tracksResponse = listPage.items;
                                 
                                 if (tracksResponse)
                                 {
                                     for (SPTPartialTrack *partialTrack in tracksResponse)
                                     {
                                         if ([partialTrack.name.uppercaseString isEqualToString:songTrack.name.uppercaseString])
                                         {
                                             songTrack.spotifyTrackURI = partialTrack.uri.absoluteString;
                                             break;
                                         }
                                     }
                                     
                                     if (songTrack.spotifyTrackURI.length == 0)
                                     {
                                         SPTPartialTrack *partialTrack = tracksResponse.firstObject;
                                         songTrack.spotifyTrackURI = partialTrack.uri.absoluteString;
                                     }
                                 }
                                 else
                                 {
                                     //if its a cover search original artist
                                     if (songTrack.originalArtist.length > 0)
                                     {
                                         NSString *searchQuery = [NSString stringWithFormat:@"%@ %@", songTrack.originalArtist, songTrack.name];
                                         [SPTSearch performSearchWithQuery:searchQuery queryType:SPTQueryTypeTrack accessToken:auth.session.accessToken callback:^(NSError *error, id object){
                                             //no song result, search for song without artist field
                                             
                                             if (error != nil) {
                                                 NSLog(@"*** Failed to get playlist %@", error);
                                                 failure(error);
                                                 return;
                                             }
                                             else if(object)
                                             {
                                                 SPTListPage *listPage = object;
                                                 NSArray *tracksResponse = listPage.items;
                                                 
                                                 //SPTPartialTrack *partialTrack = tracksResponse[0];
                                                 if (tracksResponse)
                                                 {
                                                     for (SPTPartialTrack *partialTrack in tracksResponse)
                                                     {
                                                         if ([partialTrack.name.uppercaseString isEqualToString:songTrack.name.uppercaseString])
                                                         {
                                                             songTrack.spotifyTrackURI = partialTrack.uri.absoluteString;
                                                             break;
                                                         }
                                                     }
                                                     
                                                     if (songTrack.spotifyTrackURI.length == 0)
                                                     {
                                                         SPTPartialTrack *partialTrack = tracksResponse.firstObject;
                                                         songTrack.spotifyTrackURI = partialTrack.uri.absoluteString;
                                                     }
                                                 }
                                                 else
                                                 {
                                                     //cant find that track, put in empty string
                                                     songTrack.spotifyTrackURI = @"notAvailable";
                                                     //count++;
                                                 }
                                                 
                                                 if ([setlist allSongsCheckedForSpotifyURI])
                                                 //if (count == [setlist arrayOfAllSongs].count)
                                                 {
                                                     NSMutableArray *tracksURIsArray = [[NSMutableArray alloc] initWithArray:@[]];
                                                     for (Song *song in [setlist arrayOfAllSongs])
                                                     {
                                                         if (song.spotifyTrackURI.length > 0 && ![song.spotifyTrackURI isEqualToString:@"notAvailable"])
                                                         {
                                                             [tracksURIsArray addObject:[NSURL URLWithString:song.spotifyTrackURI]];
                                                         }
                                                     }
                                                     
                                                     [playlist addTracksToPlaylist:tracksURIsArray withSession:auth.session callback:^(NSError *error) {
                                                         if (error != nil) {
                                                             NSLog(@"*** Failed to add tracks to playlist %@", error);
                                                             failure(error);
                                                             return;
                                                         }
                                                         else
                                                         {
                                                             success();
                                                         }
                                                     }];
                                                 }
                                             }
                                         }];
                                     }
                                     else
                                     {
                                         //cant find that track, put in empty string
                                         songTrack.spotifyTrackURI = @"notAvailable";
                                         //count++;
                                     }
                                 }
                                 
                                 if ([setlist allSongsCheckedForSpotifyURI])
                                 //if (count == [setlist arrayOfAllSongs].count)
                                 {
                                     NSMutableArray *tracksURIsArray = [[NSMutableArray alloc] initWithArray:@[]];
                                     for (Song *song in [setlist arrayOfAllSongs])
                                     {
                                         if (song.spotifyTrackURI.length > 0 && ![song.spotifyTrackURI isEqualToString:@"notAvailable"])
                                         {
                                             [tracksURIsArray addObject:[NSURL URLWithString:song.spotifyTrackURI]];
                                         }
                                     }
                                     
                                     [playlist addTracksToPlaylist:tracksURIsArray withSession:auth.session callback:^(NSError *error) {
                                         if (error != nil) {
                                             NSLog(@"*** Failed to add tracks to playlist %@", error);
                                             failure(error);
                                             return;
                                         }
                                         else
                                         {
                                             success();
                                         }
                                     }];
                                 }
                             }
                         }];
                     }
                 }
             }];
        }
    }];
}

@end
