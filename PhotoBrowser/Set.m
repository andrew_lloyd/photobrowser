#import "Set.h"
#import "Song.h"

@interface Set ()

// Private interface goes here.

@end

@implementation Set

// Custom logic goes here.
- (void)populateWithJSON:(NSDictionary*)json inContext:(NSManagedObjectContext*)context
{
    NSDictionary *songs = [json valueForKeyPath:@"song"];
    
    if ([json valueForKeyPath:@"encore"] > 0)
    {
        self.encore = [NSNumber numberWithBool:YES];
    }
    
    if (songs.count > 1)
    {
        for (NSDictionary *songJSON in songs)
        {
            if([songJSON isKindOfClass:[NSString class]])
            {
                Song *song = [NSEntityDescription
                              insertNewObjectForEntityForName:@"Song"
                              inManagedObjectContext:context];
                
                [song populateWithJSON:songs inContext:context];
                [self.containsSongsSet addObject:song];
            }
            else
            {
                Song *song = [NSEntityDescription
                              insertNewObjectForEntityForName:@"Song"
                              inManagedObjectContext:context];
                
                [song populateWithJSON:songJSON inContext:context];
                [self.containsSongsSet addObject:song];
            }
        }
    }
    else
    {
        Song *song = [NSEntityDescription
                      insertNewObjectForEntityForName:@"Song"
                      inManagedObjectContext:context];
        
        [song populateWithJSON:songs inContext:context];
        [self.containsSongsSet addObject:song];
    }
}

@end
