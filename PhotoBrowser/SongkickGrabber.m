//
//  EventGrabber.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 20/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import "SongkickGrabber.h"
#import "SongkickAPIKey.h"
#import "NSDate+SameDay.h"


@implementation SongkickGrabber

@synthesize venueID, events, eventName, eventArtists;

- (void)fetchEvent:(NSArray *)placeQueries onDate:(NSDate *)theDate;
{
    //unimplemented but could for loop round for multiple concerts if in the location of more than one
    //for (NSString *query in placeQueries) {
    
    // create a (non-main) queue to do fetch on
    //dispatch_queue_t fetchQ = dispatch_queue_create("venueID grabber", NULL);
    // put a block to do the fetch onto that queue
    //dispatch_async(fetchQ, ^{
    
    NSDictionary *venueListResults = nil;
    
    for (int i = 0; i < placeQueries.count; i++){
        
        NSString *query =[placeQueries objectAtIndex:i];
        //Songkick
        //NSString *urlString = [NSString stringWithFormat:@"http://api.songkick.com/api/3.0/search/venues.json?query=%@&apikey=%@", query, SongkickAPIKey];

        //LastFm
        NSString *urlString = [NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=venue.search&api_key=%@&venue=%@&format=json", LastFmAPIKey, query];
        
        NSURL *theURL = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        
        // fetch the JSON data from Google
        NSData *jsonResults = [NSData dataWithContentsOfURL:theURL];
        if (jsonResults != nil)
        {
            // convert it to a Property List (NSArray and NSDictionary)
            venueListResults = [NSJSONSerialization JSONObjectWithData:jsonResults
                                                               options:0
                                                                 error:NULL];
            NSLog(@"%@", venueListResults);
            NSLog(@"Did you get this far?!");
            
            /// That api returns an array of possible venues.
            
            //if no venue matches the place name
            if([[venueListResults valueForKeyPath:@"results.venuematches"] isKindOfClass:[NSString class]]) {
                NSLog(@"This wont work");
            }else{
                break;
            }

        }
    }
    
    id result = [venueListResults valueForKeyPath:@"results.venuematches.venue.id"];
    NSLog(@"%@", result);
    
    if([result isKindOfClass:[NSArray class]]) {
        //NSNumber *venueID_asANumber = [result firstObject];
        //venueID = [venueID_asANumber stringValue];
        venueID = [result firstObject];
    }
    else if([result isKindOfClass:[NSString class]]) {
        venueID = result;
    }
    NSLog(@"%@", venueID);
    ////
    
    NSDate *today = [NSDate date];
    
    NSString *listingsURLString = [[NSString alloc]init];
    
    //if the photo was taken today get current events, if not get venues past events
    if ([theDate isSameDayAsDate:today]) {
        listingsURLString = [NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=venue.getevents&api_key=%@&venue=%@&format=json", LastFmAPIKey, venueID];
    }else{
        //LastFm
        listingsURLString = [NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=venue.getpastevents&api_key=%@&venue=%@&format=json", LastFmAPIKey, venueID];
    }
    
    NSURL *theListingsURL = [NSURL URLWithString:[listingsURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    // fetch the JSON data from Google
    NSData *jsonListingResults = [NSData dataWithContentsOfURL:theListingsURL];
    // convert it to a Property List (NSArray and NSDictionary)
    NSDictionary *ListingsResults = [NSJSONSerialization JSONObjectWithData:jsonListingResults
                                                                    options:0
                                                                      error:NULL];
    NSLog(@"%@", ListingsResults);
    
    events = [ListingsResults valueForKeyPath:@"events.event"];
    
    NSLog(@"%@", events);
    
    
    NSLog(@"Done");
    
    //uses unicode date format settings http://unicode.org/reports/tr35/tr35-6.html#Date_Format_Patterns
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEE, dd MMM yyyy"];
    NSString *dayName = [dateFormatter stringFromDate:theDate];
    NSLog(@"%@", dayName);
    
    for (int i = 0; i < events.count; i++)
    {
        NSString *dateOfGig = [events[i] valueForKeyPath:@"startDate"];
        if ([dateOfGig hasPrefix:(dayName)])
        {
            NSLog(@"Heres what you did: %@", [events[i] valueForKeyPath:@"startDate"]);
            eventName = [events[i] valueForKeyPath:@"artists.headliner"];
            NSLog(@"The band your photo is of is %@", eventName);
            //if ([[events[i] valueForKeyPath:@"artists.artist"] count] > 1)
            //{
            eventArtists = [events[i] valueForKeyPath:@"artists.artist"];
            NSLog(@"%@", eventArtists);
            //}
            break;
        }
    }
    
    if (eventArtists == nil){
        //try it again with future events
        //needed if used after midnight - previous loop checks past events if the time is past midnight
        //but the event is still listed as a future event until morning
        //LastFm
        listingsURLString = [NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=venue.getevents&api_key=%@&venue=%@&format=json", LastFmAPIKey, venueID];
        
        
        NSURL *theListingsURL = [NSURL URLWithString:[listingsURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        // fetch the JSON data from Google
        NSData *jsonListingResults = [NSData dataWithContentsOfURL:theListingsURL];
        // convert it to a Property List (NSArray and NSDictionary)
        NSDictionary *ListingsResults = [NSJSONSerialization JSONObjectWithData:jsonListingResults
                                                                        options:0
                                                                          error:NULL];
        NSLog(@"%@", ListingsResults);
        
        events = [ListingsResults valueForKeyPath:@"events.event"];
        
        NSLog(@"%@", events);
        
        
        NSLog(@"Done");
        
        //uses unicode date format settings http://unicode.org/reports/tr35/tr35-6.html#Date_Format_Patterns
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"EEE, dd MMM yyyy"];
        NSString *dayName = [dateFormatter stringFromDate:theDate];
        NSLog(@"%@", dayName);
        
        for (int i = 0; i < events.count; i++)
        {
            NSString *dateOfGig = [events[i] valueForKeyPath:@"startDate"];
            if ([dateOfGig hasPrefix:(dayName)])
            {
                NSLog(@"Heres what you did: %@", [events[i] valueForKeyPath:@"startDate"]);
                eventName = [events[i] valueForKeyPath:@"artists.headliner"];
                NSLog(@"The band your photo is of is %@", eventName);
                //if ([[events[i] valueForKeyPath:@"artists.artist"] count] > 1)
                //{
                eventArtists = [events[i] valueForKeyPath:@"artists.artist"];
                NSLog(@"%@", eventArtists);
                //}
                break;
            }
        }
    }
    
    if(eventArtists == nil)
    {
        //if both past events and future events fail, we don't know about that event
        eventName = @"Unknown Event";
    }
    
    
}

-(NSString*)getEvent
{
    return eventName;
}

-(NSArray*)getArtists
{
    return eventArtists;
}

@end
