//
//  FlickrResultsTableViewController.h
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 17/03/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrResultsTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *photos; // of Flickr photo NSDictionary
@property (nonatomic, strong) NSString *query; //the text the user has searched
@property (nonatomic, strong) NSDate *dateTaken; //date of event

@end
