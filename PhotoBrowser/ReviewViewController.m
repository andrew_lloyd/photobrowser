//
//  ReviewViewController.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 05/06/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import "ReviewViewController.h"

@interface ReviewViewController ()

@end

@implementation ReviewViewController

@synthesize theURLquery, theWebView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSURLRequest *myRequest = [NSURLRequest requestWithURL:theURLquery];
    
    [theWebView loadRequest:myRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
