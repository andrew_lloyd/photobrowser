//
//  SetlistSearchViewController.m
//  PhotoBrowser
//
//  Created by Andrew Lloyd on 01/04/2014.
//  Copyright (c) 2014 Andrew Lloyd. All rights reserved.
//

#import "SetlistSearchViewController.h"
#import "HomeViewController.h"

@interface SetlistSearchViewController ()

@end

@implementation SetlistSearchViewController

@synthesize artistField, venueField, dateField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //set nav bar properties
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor], NSFontAttributeName: [UIFont fontWithName:@"American Typewriter" size:22.0]};
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:36.0 / 255.0  green:115.0/255.0 blue:187.0/255.0 alpha:1.0];
    
    // Do any additional setup after loading the view.
    artistField.clearButtonMode = UITextFieldViewModeWhileEditing;
    venueField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    NSInteger randomNumber = arc4random_uniform(3);
    NSString *imagename = [NSString stringWithFormat:@"%li", (long)randomNumber];
    self.backgroundImageView.image = [UIImage imageNamed:imagename];
    
    self.artistField.delegate = self;
    self.venueField.delegate = self;
    
    [self initializeTextFieldInputView];
    [self roundPopupViewCorners];
}

- (void)roundPopupViewCorners
{
    self.popupView.layer.cornerRadius = 10;
    //	self.clipsToBounds = YES;
    self.popupView.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor blackColor]);
    self.popupView.layer.borderWidth = 2;
    
    self.popupView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.popupView.layer.shadowOffset = CGSizeMake(0, 2);
    self.popupView.layer.shadowOpacity = 1;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.popupBorderView.backgroundColor = [UIColor darkGrayColor];
    self.popupBorderView.layer.cornerRadius = 10;
    self.popupBorderView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    self.popupBorderView.layer.shadowOffset = CGSizeMake(0, 2);
    self.popupBorderView.layer.shadowOpacity = 0.9;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"goHome"])
    {
        //do nothing
    }
    else
    {
        [self prepareSetlistViewController:segue.destinationViewController];
    }
}


- (void)prepareSetlistViewController:(SetlistViewController *)svc
{
    NSString *artistQuery = artistField.text;
    artistQuery = [artistQuery stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *venueQuery = venueField.text;
    venueQuery = [venueQuery stringByReplacingOccurrencesOfString:@" " withString:@"+"];

    svc.artist = artistField.text;
    svc.venue = venueField.text;
    svc.dateOfShow = dateField.text;
    
    svc.title = @"Setlist";
}

- (IBAction)searchBtn:(id)sender {
    if ([artistField.text isEqualToString:@""]) {
        UIAlertView *noArtistEntered = [[UIAlertView alloc] initWithTitle:@"No Artist Entered"
                                                                  message:@"You must enter an artist to search setlists."
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
        [noArtistEntered show];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        SetlistViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"setlistViewController"];
        [self prepareSetlistViewController:viewController];
        
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [artistField resignFirstResponder];
    [venueField resignFirstResponder];
    [dateField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //handle search or push to next field
    if (textField == self.artistField)
    {
        if (self.venueField.text.length > 0)
        {
            [self searchBtn:nil];
        }
        else
        {
            [self.venueField becomeFirstResponder];
        }
    }
    if (textField == self.venueField)
    {
        if (self.dateField.text.length > 0)
        {
            [self searchBtn:nil];
        }
        else
        {
            [self.dateField becomeFirstResponder];
        }
    }
    
    if (textField) {
        [textField resignFirstResponder];
    }
    
    return NO;
}

- (void) initializeTextFieldInputView {
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.minuteInterval = 5;
    datePicker.backgroundColor = [UIColor whiteColor];
    [datePicker addTarget:self action:@selector(dateUpdated:) forControlEvents:UIControlEventValueChanged];
    self.dateField.inputView = datePicker;
}

- (void) dateUpdated:(UIDatePicker *)datePicker {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    self.dateField.text = [formatter stringFromDate:datePicker.date];
}

@end
